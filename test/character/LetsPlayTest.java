package character;

import Classes.Mage;
import Equipment.Armor.Armour;
import Equipment.Items.Item;
import Equipment.Weapon.Weapon;
import exceptions.InvalidArmourException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class LetsPlayTest {
   public void charactersShouldStart_atLevel1(){
       String name = "Kiwi";
       Character Mage = new Mage("nameOfCharacter");
       int expected = 1;
       int current = Mage.getLevel();

       assertEquals(1, current);

   }

   public void GameCharacter_levelIncreaseBy1(){
       String name = "Kiwi";
       Character Mage = new Mage("nameOfCharacter");
       int expected = 2;
       int current = Mage.getLevel();

       assertEquals(1, current);



   }

   public void GameCharacter_equipValidWeapon_shouldReturnValidWeapon () throws InvalidLevelException, InvalidWeaponException {
       String name = "Kiwi";
       Character Mage = new Mage("nameOfCharacter");
       String nameOfWeapon = "common staff";

       PrimaryAttribute weaponAttribute  = new PrimaryAttribute(1,1,1);
       int requiredLevel= 1;
       Weapon.WeaponType WeaponType = Weapon.WeaponType.Staffs;
       int damageOfWeapon = 10;
       double attackSpeedOfWeapon = 0.5;

       Weapon commonStaff = new Weapon(
               nameOfWeapon,
               weaponAttribute,
               requiredLevel,
               WeaponType,
               damageOfWeapon,
               attackSpeedOfWeapon);

       boolean expected = true;
       boolean actual = Mage.equipWeapon(commonStaff);
       assertEquals(expected, actual);


   }

    @Test
    public void equipWeapon_invalidWeaponType_shouldThrowWeaponException() {

        String  nameOfCharacter = "Kiwi";
        Character mage = new Mage(nameOfCharacter);

        String nameOfWeapon = "Bows";
        PrimaryAttribute weaponAttributes = new PrimaryAttribute(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Bows;
        int damageOfWeapon = 15;
        double attackSpeedOfWeapon = 0.2;

        Weapon bows = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        assertThrows(InvalidWeaponException.class, () -> mage.equipWeapon(bows));
    }



    @Test
    public void equipArmour_validArmour_shouldReturnTrueIfEquipped() throws InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Kiwi";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "Common Jacket";
        PrimaryAttribute armourAttributes = new PrimaryAttribute(1, 1, 3);
        int requiredLevel = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlots armourSlot = Item.ItemSlots.Body;



        Armour commonJacket = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        boolean expected = true;
        boolean actual = mage.equipArmour(commonJacket);

        assertEquals(expected,actual);
    }
    @Test
    public void equipArmour_invalidArmourLevel_shouldReturnLevelException(){

        String  nameOfCharacter = "Kiwi";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "common Jacket";
        PrimaryAttribute armourAttributes = new PrimaryAttribute(1, 1, 3);
        int requiredLevel = 2;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlots armourSlot = Item.ItemSlots.Body;



        Armour commonJacket = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        Exception exception = assertThrows(InvalidLevelException.class, () -> mage.equipArmour(commonJacket));

        String expected = "A level \"1\"  the payer cannot equip with level current requirement \"2\"";
        String actual = exception.getMessage();

        //assertEquals(expected,actual);
    }

    @Test
    public void equipArmour_invalidArmour_shouldReturnLevelException() {

        String  nameOfCharacter = "Kiwi";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "common Robe";
        PrimaryAttribute armourAttributes = new PrimaryAttribute(1, 3, 1);
        int requiredLevel = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Leather;
        Item.ItemSlots armourSlot = Item.ItemSlots.Body;



        Armour commonRobe = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        assertThrows(InvalidArmourException.class, () -> mage.equipArmour(commonRobe));
    }


}







