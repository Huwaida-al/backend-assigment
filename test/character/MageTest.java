package character;

import Classes.Mage;
import Equipment.Armor.Armour;
import Equipment.Items.Item;
import Equipment.Weapon.Weapon;
import exceptions.InvalidArmourException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class MageTest {


    //Character level should begin with level 1

    @Test
    public void newCharacter_ShouldHave_baseAttributes() {

        String nameOfCharacter = "Kiwi";
        Character Mage = new Mage(nameOfCharacter);

        PrimaryAttribute attribute = new PrimaryAttribute(1, 1, 8);
        double expected = 1 * (1 + (attribute.getIntelligence() / 100));
        double current = Mage.totalAttributes.getStrength ();

        //validate the two values
        assertEquals(1, Mage.totalAttributes.getStrength());
        assertEquals(1, Mage.totalAttributes.getDexterity());
        assertEquals(8, Mage.totalAttributes.getIntelligence());

    }

    @Test

    public void checkDps_Weapon_tooHigh() throws InvalidLevelException, InvalidWeaponException {

        String nameOfCharacter = "Kiwi";
        Character Mage = new Mage(nameOfCharacter);

        PrimaryAttribute attribute = new PrimaryAttribute(1, 1, 8);

        String nameOfWeapon = "common Staff";
        PrimaryAttribute weaponAttribute = new PrimaryAttribute(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType WeaponType = Weapon.WeaponType.Staffs;


        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon commonStaff = new Weapon(
                nameOfWeapon,
                weaponAttribute,
                requiredLevel,
                WeaponType,
                damageOfWeapon,
                attackSpeedOfWeapon
        );

                Mage.equipWeapon(commonStaff);
                double expected = 1* (1 +(attribute.getIntelligence()/100));
                double current = Mage.getDps();

                assertEquals(5, current);

    }

    @Test

    public void checkDps_weaponAndArmour_high()throws InvalidArmourException, InvalidWeaponException, InvalidLevelException {

        String nameOfCharacter = "Kiwi";
        String nameOfWeapon = "common staff";

        PrimaryAttribute attribute = new PrimaryAttribute(1, 1, 8);


        PrimaryAttribute weaponAttribute = new PrimaryAttribute(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType WeaponType = Weapon.WeaponType.Staffs;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;


        String nameOfArmour = "common Jacket";
        PrimaryAttribute armourAttributes = new PrimaryAttribute(1, 1, 3);
        int requiredLevelArmour = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlots armourSlot = Item.ItemSlots.Body;

        Weapon commonStaff = new Weapon(
                nameOfWeapon,
                weaponAttribute,
                requiredLevel,
                WeaponType,
                damageOfWeapon,
                attackSpeedOfWeapon
        );

        Armour commonJacket = new Armour(
                nameOfArmour,
                requiredLevelArmour,
                armourAttributes,
                armourType,
                armourSlot
        );

         Character Mage = new Mage (nameOfCharacter);
         Mage.equipWeapon(commonStaff);
         Mage.equipArmour(commonJacket);

         PrimaryAttribute attributes = new PrimaryAttribute(1,1,8);
                 attributes.characterAttributes(1,1,3);

         double expected = commonStaff.dps * (1 +(attribute.getIntelligence()/ 100));
         double current = Mage.getDps();

         assertEquals(expected, current);



    }


}



