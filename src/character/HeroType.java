package character;

public enum HeroType {
    Mage,
    Ranger,
    Rogue,
    Warrior
}
