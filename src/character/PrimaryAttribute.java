package character;

public class PrimaryAttribute {
    public PrimaryAttribute (int strength, int dexterity, int intelligence){
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence= intelligence;

    }
    private int strength;
    private int dexterity;
    private int intelligence;

    //getters/setters

    public int getStrength(){
        return strength;
    }

    public void setStrength (int strength){
        this.strength = strength;
    }
    public int getDexterity(){
   return dexterity;
    }
    public void setDexterity(int dexterity){
        this.dexterity = dexterity;


    }
    public int getIntelligence(){
        return intelligence;
    }
    public void setIntelligence(int intelligence){
        this.intelligence = intelligence;
    }


    public void characterAttributes(int strength, int dexterity, int intelligence){
        this.setStrength(this.getStrength() + strength);
        this.setDexterity(this.getDexterity() + dexterity);
        this.setIntelligence(this.getIntelligence() + intelligence);

    }
    public String characterStats(){
        return ", Strength: "+this.getStrength() +", Dexterity: "+ this.getDexterity() +", Intelligence: "+ this.getIntelligence();
    }
}



