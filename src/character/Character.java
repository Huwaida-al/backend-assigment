package character;

import Equipment.Armor.Armour;
import Equipment.Items.Item;
import Equipment.Weapon.Weapon;
import exceptions.InvalidArmourException;
import exceptions.InvalidLevelException;
import exceptions.InvalidWeaponException;


import java.util.Arrays;
import java.util.HashMap;


public abstract class Character {
    private HeroType heroType;
    public PrimaryAttribute baseAttributes;
    public PrimaryAttribute totalAttributes;

    public HashMap<Item.ItemSlots, Item> equipment;
    public Armour.ArmourType[] AllowedArmour;
    public Weapon.WeaponType[] AllowedWeapon;

    public Character(String name, int level) {
        this.name = name;
        this.level = level;
    }


    public String display() {
        return name;
    }

    //getting the name, level and dps
    private String name;

    public String getName() {
        return name;

    }

    private int level;
    public int getLevel() {
        return level;
    }

    public double dps;

    public double getDps() {
        return dps;
    }
    public HeroType getHeroType() {
        return heroType;
    }

    public void setHeroType(HeroType heroType) {
        this.heroType = heroType;
    }



    //Creating Constructor for the characters

    public Character(String name, HeroType heroType, int strength, int dexterity, int intelligence) {
        this.name = name;
        this.heroType = heroType;
        this.level = 1;

        baseAttributes = new PrimaryAttribute(strength, dexterity, intelligence);
        totalAttributes = new PrimaryAttribute(strength, dexterity, intelligence);
        this.equipment = new HashMap<>();

        calculateDps();

    }
    // Method that all the characters got

    public abstract void Level1Up();
    public void gainLevel1() {
        this.level++;
    }
    public abstract void calculateDps();

    // display stats

    public void displayStats() {
        System.out.println("Name: " + this.getName());
        System.out.println("Class: " + this.getHeroType());
        System.out.println("Level: " + this.getLevel());
        System.out.println("Strength: " + this.totalAttributes.getStrength());
        System.out.println("Dexterity: " + this.totalAttributes.getDexterity());
        System.out.println("Intelligence: " + this.totalAttributes.getIntelligence());

    }

    // function to equip armour
    public boolean equipArmour(Armour armour) throws InvalidArmourException, InvalidLevelException {
        boolean levelLow = this.level < armour.getCharacterLevel();
        boolean invalidArmourType = Arrays.stream(AllowedArmour).noneMatch(type -> type == armour.getType());


        if (invalidArmourType) {
            throw new InvalidArmourException(heroType.toString(), armour.getType().toString());
        } else if (levelLow) {
            throw new InvalidLevelException(this.getLevel(), armour.getCharacterLevel());
        }
        equipment.put(armour.getSlots(), armour);
//        calculateDps();
        return true;
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException, InvalidLevelException {
        boolean levelTooLow = this.level < weapon.getCharacterLevel();
        boolean invalidWeaponType = Arrays.stream(AllowedWeapon).noneMatch(type -> type == weapon.getType());
        if (invalidWeaponType) {
            throw new InvalidWeaponException(heroType.toString(), weapon.getType().toString());
        } else if (levelTooLow) {
            throw new InvalidLevelException(this.getLevel(), weapon.getCharacterLevel());
        }
        equipment.put(weapon.getSlots(), weapon);
        CalculateAttributes();

        return true;
    }

    private void CalculateAttributes() {
        this.totalAttributes = new PrimaryAttribute(0, 0, 0);

        this.totalAttributes.characterAttributes(baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence());

        this.equipment.forEach((slot, item) -> {
            if (slot == Item.ItemSlots.Weapon) return;
            if (item == null) return;
            PrimaryAttribute itemAttributes = item.getAttributes();
            this.totalAttributes.characterAttributes(itemAttributes.getStrength(), itemAttributes.getDexterity(), itemAttributes.getIntelligence());
        });
        calculateDps();
    }

}


