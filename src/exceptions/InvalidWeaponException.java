package exceptions;

public class InvalidWeaponException extends Exception{
    public InvalidWeaponException(String profession, String weaponType) {
       super("\""+ profession+ "\" you cannot access the weapon \"" + weaponType + "\"");
    }
}



