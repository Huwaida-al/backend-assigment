package exceptions;

public class InvalidLevelException extends Exception{
    public InvalidLevelException(int playerLevel, int levelRequirment) {
        super("A level \"" + playerLevel+ "\" player cannot equip gear with level requirement of level \"" + levelRequirment + "\"");
    }
}

