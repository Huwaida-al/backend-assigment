package Classes;

import Equipment.Armor.Armour;
import Equipment.Items.Item;
import Equipment.Weapon.Weapon;
import character.Character;
import character.HeroType;

public class Mage extends Character {
    private static final int STARTING_STRENGTH = 1;
    private static final int STARTING_DEXERITY = 1;
    private static final int STARTING_INTELLIGENCE = 8;
    private static final int LEVEL_STRENGTH = 1;
    private static final int LEVEL_DEXTERITY = 1;
    private static final int LEVEL_INTELLIGENCE = 5;

    public Mage(String name) {
        super(name, HeroType.Mage, STARTING_STRENGTH, STARTING_DEXERITY, STARTING_INTELLIGENCE);
 AllowedArmour = new Armour.ArmourType[]{Armour.ArmourType.Cloth};
       AllowedWeapon = new Weapon.WeaponType[]{Weapon.WeaponType.Staffs, Weapon.WeaponType.Wands};

    }

    @Override
    public void Level1Up() {
        this.getLevel();
        this.baseAttributes.characterAttributes(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);

    }

    @Override
    public void calculateDps() {
//        double set = 1;
        double dps = 1;

        if (equipment.get(Item.ItemSlots.Weapon) == null) {
            return;
        }


        Weapon weapon = (Weapon) equipment.get(Item.ItemSlots.Weapon);
        dps = weapon.getDps();
        this.dps =(dps *(1 +(totalAttributes.getIntelligence()/100)));

    }


}





