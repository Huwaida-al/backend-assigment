package Classes;

import Equipment.Armor.Armour;
import Equipment.Items.Item;
import Equipment.Weapon.Weapon;
import character.Character;
import character.HeroType;

public class Rouger extends Character {
    private static final int  STARTING_STRENGTH = 2;
    private static final int STARTING_DEXERITY = 6;
    private static final int STARTING_INTELLIGENCE= 1;

    private static final int LEVEL_STRENGTH = 1;
    private static final int LEVEL_DEXTERITY= 4;
    private static final int LEVEL_INTELLIGENCE= 1;

    public Rouger (String name){
        super(name, HeroType.Mage, STARTING_STRENGTH, STARTING_DEXERITY, STARTING_INTELLIGENCE);
        this.AllowedArmour  = new  Armour.ArmourType[]{ Armour.ArmourType.Mail };
        this.AllowedWeapon = new Weapon.WeaponType[] { Weapon.WeaponType.Daggers, Weapon.WeaponType.Swords };

    }


    @Override
    public void Level1Up() {
        this.getLevel();
        this.baseAttributes.characterAttributes(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);

    }

    @Override
    public void calculateDps() {
        double dps = 1;

        if (equipment.get(Item.ItemSlots.Weapon) == null) {
            return;

        }
        Weapon weapon = (Weapon)equipment.get(Item.ItemSlots.Weapon);
        dps = weapon.getDps();

        this.dps = (dps * (1 + (totalAttributes.getDexterity() / 100)));

    }


}













