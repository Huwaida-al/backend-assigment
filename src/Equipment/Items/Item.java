package Equipment.Items;

import character.PrimaryAttribute;

public abstract class Item {
    public enum ItemSlots {
        Head,
        Body,
        Legs,
        Weapon

    }
    private final String name;
    public PrimaryAttribute attributes;
    private final int characterLevel;
    private final ItemSlots slots;

    public String getName(){
        return name;
    }
    public  PrimaryAttribute getAttributes (){
        return attributes;
    }
    public  int getCharacterLevel(){
        return characterLevel;
    }
    public ItemSlots getSlots(){
        return slots;
    }
    public Item(String name, int characterLevel, PrimaryAttribute  attributes, ItemSlots slots){
        this.name = name;
        this.attributes = attributes;
        this.characterLevel = characterLevel;
        this.slots = slots;
    }

}
