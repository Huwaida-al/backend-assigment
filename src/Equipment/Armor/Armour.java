package Equipment.Armor;


import Equipment.Items.Item;
import character.PrimaryAttribute;

public class Armour extends Item {

    public enum ArmourType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    private PrimaryAttribute attributes;

    private ArmourType type;

    public PrimaryAttribute getAttributes(){
        return attributes;

    }
    public void setAttributes(PrimaryAttribute attributes) {
        this.attributes = attributes;

    }
    public ArmourType getType(){
        return type;
    }
    public void setType(ArmourType type){
        this.type = type;
    }


    public Armour(String name, int requiredLevel, PrimaryAttribute attributes, ArmourType type, ItemSlots slots) {
        super(name, requiredLevel, attributes, slots);
        this.attributes = attributes;
        this.type = type;
    }
}