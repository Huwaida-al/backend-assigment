package Equipment.Weapon;

import Equipment.Items.Item;
import character.PrimaryAttribute;

public class  Weapon extends Item {


    public enum WeaponType{
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands
    }

    public WeaponType type;
    public WeaponType getType() {
        return type;
    }
    public double damage;
    public double attackSpeed;
    public double dps;
    public double getDps(){

    return dps;
}
public Weapon(String name, PrimaryAttribute attributes, int characterLevel, WeaponType type, double damage, double attackSpeed ){
    super(name, characterLevel, attributes, ItemSlots.Weapon);
    this.type= type;
    this.attackSpeed= attackSpeed;
    this.damage= damage;
    this.dps = damage * attackSpeed;
}
}